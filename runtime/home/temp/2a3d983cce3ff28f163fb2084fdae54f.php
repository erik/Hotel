<?php /*a:3:{s:54:"D:\phpstudy_pro\WWW\tp\view\home\nightaudit\index.html";i:1603150167;s:51:"D:\phpstudy_pro\WWW\tp\view\home\common\static.html";i:1603931011;s:54:"D:\phpstudy_pro\WWW\tp\view\home\common\resources.html";i:1603609812;}*/ ?>
<!DOCTYPE html>
<html class="x-admin-sm">

<head>
    <meta charset="UTF-8">
    <title><?php echo htmlentities($system['hotel_name']); ?>(多酒店版)</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi" />
    <link rel="stylesheet" href="/static/admin/css/font.css">
    <link rel="stylesheet" href="/static/admin/css/xadmin.css">
    <script src="/static/admin/lib/layui/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="/static/admin/js/xadmin.js"></script>

    <script src="https://cdn.bootcdn.net/ajax/libs/jquery/2.0.3/jquery.js"></script>
    <script src="/static/jquery.printarea.js"></script>

    <!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
    <!--[if lt IE 9]>
    <script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
    <script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


    <link href="/static/toastr/toastr.css" rel="stylesheet"/>
    <script src="/static/toastr/toastr.js"></script>

</head>
<input type="hidden" value="<?php echo htmlentities($voice['types']); ?>" id="voice">

<script>
    //语音播报
    function voice(name) {
        //判断语音是否开启
        if(<?php echo htmlentities($voice['status']); ?> === '0'){
            return false;
        }
        if($('#voice').val() === '思悦'){
            var audio= new Audio("/static/voice/siyue/"+name+".mp3");
        }else if($('#voice').val() === '若兮'){
            var audio= new Audio("/static/voice/ruoxi/"+name+".mp3");
        }else if($('#voice').val() === '艾琪'){
            var audio= new Audio("/static/voice/aiqi/"+name+".mp3");
        }else if($('#voice').val() === '艾美'){
            var audio= new Audio("/static/voice/aimei/"+name+".mp3");
        }else if($('#voice').val() === '艾悦'){
            var audio= new Audio("/static/voice/aiyue/"+name+".mp3");
        }else if($('#voice').val() === '青青'){
            var audio= new Audio("/static/voice/qingqing/"+name+".mp3");
        }else if($('#voice').val() === '翠姐'){
            var audio= new Audio("/static/voice/cuijie/"+name+".mp3");
        }else if($('#voice').val() === '姗姗'){
            var audio= new Audio("/static/voice/shanshan/"+name+".mp3");
        }else if($('#voice').val() === '小玥'){
            var audio= new Audio("/static/voice/xiaoyue/"+name+".mp3");
        }
        audio.play();//播放
    }
</script>
<!--<link href="https://cdn.bootcdn.net/ajax/libs/twitter-bootstrap/3.4.0/css/bootstrap.css" rel="stylesheet">-->
<!--<link rel="stylesheet" href="/static/bootstrap/css/bootstrap.css">-->
<link href="/static/bootstrap3.0.css" rel="stylesheet" type="text/css"/>
<script src="/static/bootstrap/js/bootstrap.js"></script>
    <body>
        <div class="x-nav">
            <span class="layui-breadcrumb">
                <a href="">首页</a>
                <a href="">演示</a>
                <a>
                    <cite>导航元素</cite></a>
            </span>
            <a class="layui-btn layui-btn-small" style="line-height:1.6em;margin-top:3px;float:right" onclick="location.reload()" title="刷新">
                <i class="layui-icon layui-icon-refresh" style="line-height:30px"></i>
            </a>
        </div>
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <div class="layui-col-md12">
                    <div class="layui-card">

                        <div class="layui-card-body ">

                            <form class="layui-form" action="" method="post">

                                <div class="layui-form-item">
                                    <label class="layui-form-label">应到未到</label>
                                    <div class="layui-input-block">
                                        <input type="radio" name="future" value="1" title="开启" <?php if($list['future'] == '1'): ?>checked <?php endif; ?>>
                                        <input type="radio" name="future" value="0" title="关闭" <?php if($list['future'] == '0'): ?>checked <?php endif; ?>>
                                    </div>
                                    <div class="layui-form-mid layui-word-aux">如果开启，则不处理应到未到订单将无法进行夜审</div>
                                </div>

                                <div class="layui-form-item">
                                    <label class="layui-form-label">应离未离</label>
                                    <div class="layui-input-block">
                                        <input type="radio" name="leaving" value="1" title="开启" <?php if($list['leaving'] == '1'): ?>checked <?php endif; ?>>
                                        <input type="radio" name="leaving" value="0" title="关闭" <?php if($list['leaving'] == '0'): ?>checked <?php endif; ?>>
                                    </div>
                                    <div class="layui-form-mid layui-word-aux">如果开启，则不处理应离未离订单将无法进行夜审</div>
                                </div>

<!--                                <div class="layui-form-item">
                                    <label class="layui-form-label">房间置脏</label>
                                    <div class="layui-input-block">
                                        <input type="radio" name="room_status" value="1" title="开启" <?php if($list['room_status'] == '1'): ?>checked <?php endif; ?>>
                                        <input type="radio" name="room_status" value="0" title="关闭" <?php if($list['room_status'] == '0'): ?>checked <?php endif; ?>>
                                    </div>
                                    <div class="layui-form-mid layui-word-aux">如果开启，夜审时系统把在住过夜房置为脏房</div>
                                </div>-->

<!--                                <div class="layui-form-item">
                                    <label class="layui-form-label">自动交班</label>
                                    <div class="layui-input-block">
                                        <input type="radio" name="automatic" value="1" title="开启" <?php if($list['automatic'] == '1'): ?>checked <?php endif; ?>>
                                        <input type="radio" name="automatic" value="0" title="关闭" <?php if($list['automatic'] == '0'): ?>checked <?php endif; ?>>
                                    </div>
                                    <div class="layui-form-mid layui-word-aux"> 如果开启，夜审时系统按交班规则自动交班生成交班报表</div>
                                </div>-->

                                <div class="layui-form-item">
                                    <label class="layui-form-label">夜审时间</label>
                                    <div class="layui-input-block">
                                        <div class="layui-input-inline layui-show-xs-block">
                                            <input class="layui-input" placeholder="开始日" name="night_trial_time" id="night_trial_time" value="<?php echo htmlentities($list['night_trial_time']); ?>">
                                        </div>
                                    </div>
                                    <div class="layui-form-mid layui-word-aux"> 如果开启，夜审时系统按交班规则自动交班生成交班报表</div>
                                </div>

                                <div class="layui-form-item">
                                    <div class="layui-input-block">
                                        <button class="layui-btn" lay-submit lay-filter="formDemo">保存设置</button>
                                    </div>
                                </div>
                            </form>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </body>

<script>layui.use(['laydate', 'form'],
        function() {
            var laydate = layui.laydate;

            //执行一个laydate实例
            laydate.render({
                elem: '#night_trial_time' //指定元素
                ,type: 'time'
                // ,format: 'HH点mm分' //可任意组合
                // ,range: true //或 range: '~' 来自定义分割字符
            });
        });

</script>


</html>