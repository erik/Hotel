<?php
namespace app\home\controller;


use app\index\controller\Basics;
use think\facade\Db;
use think\File;
/*
 * 系统设置
 *
 * */

class Systems extends Basics
{

    // 初始化
    protected function initialize()
    {
        //初始化模型
        parent::initialize();
    }

    /*
     * 基本设置
     * */
    public function basic(){
        if(request()->isAjax()){
            if(Db::table('hotel_system')->where('building_id',session('building_id'))->update(input('param.'))){
                return $this->return_json('保存成功','100');
            }else{
                return $this->return_json('保存失败','0');
            }
        }

        if(request()->isPost()){

            $data = input('param.');
            $data['facilities'] = implode(",",$data['facilities']);

            Db::table('hotel_system')->where('building_id',session('building_id'))->update($data);
        }
        $list = Db::table('hotel_system')->where('building_id',session('building_id'))->find();
        $facilities = $this->select_all('hotel_facilities');
        $hotel_img = Db::table('hotel_img')->where('building_id',session('building_id'))->select();
        return view('basic',['list' => $list,'facilities'=>$facilities,'hotel_img'=>$hotel_img]);
    }


    /*
     * logo图片上传
     * */
    /**
     * @return \think\response\Json
     */
    public function uploadPhoto(){
        $file = request()->file('photo');
        $savename = \think\facade\Filesystem::disk('public')->putFile( 'topic', $file);
        $savename = str_replace('\\','/',$savename);

        $http = "http://".$_SERVER['HTTP_HOST']; // "http:://www.hotel.xyz"
/*        dump($http);
        die();*/

        //删除原来的图片
        $list = Db::table('hotel_system')->where('building_id',session('building_id'))->find();
        $filename = './'.$list['logo'];
        //删除图片
        if( !empty($list['logo']) && file_exists($filename)){
            $info ='图片删除成功';
            unlink($filename);
        }else{
            $info ='图片没找到:'.$filename;
        }

        //保存数据
        Db::table('hotel_system')->where('building_id',session('building_id'))
            ->update(['logo'=>'/storage/'.$savename]);
        return json([
            'type'=>"success",
            'msg'=>'上传成功',
            'filepath'=>'/storage/'.$savename,
            'filename' =>'logo'
        ]);

    }

    /*
     * 酒店场景图片
     * */
    public function hotel_img(){
        $file = request()->file('img');
        $savename = \think\facade\Filesystem::disk('public')->putFile( 'topic', $file);
        $savename = str_replace('\\','/',$savename);
        $http = "http://".$_SERVER['HTTP_HOST'];
        $data = [
            'image'=>$http.'/storage/'.$savename,
            'src' => '/storage/'.$savename,
            'building_id'=>session('building_id')
        ];
        Db::table('hotel_img')->insert($data);

        return json([
            'type'=>"success",
            'msg'=>'上传成功',
            'filepath'=>'/storage/'.$savename,
            'filename' =>'sds'
        ]);

    }
    /*
     * 删除酒店图片
     * */
    public function remove_img(){
        $id = input('id');
        $list = Db::table('hotel_img')->where('id',$id)->find();
        $filename = './'.$list['image'];
        //删除
        if(file_exists($filename)){
            $info ='图片删除成功';
            unlink($filename);
        }else{
            $info ='图片没找到:'.$filename;
        }
        if(Db::table('hotel_img')->where('id',$id)->delete()){
            return $this->return_json($info,'100');
        }else{
            return $this->return_json($info,'0');
        }
    }

    /*
     * 设为封面图
     * */
    public function set_img(){
        $id = input('id');
        $list = Db::table('hotel_img')->where('id',$id)->find();
        dump($list);
    }

}
