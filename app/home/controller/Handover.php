<?php
namespace app\home\controller;


use app\index\controller\Basics;
use Mpdf\Mpdf;
use think\facade\Db;


/*
 * 交班管理
 *
 * */

class Handover extends Basics
{

    /*
     * 显示页面
     *
     * */
    public function index()
    {
        //查询班次
        $map = [
            ['building_id','=',session('building_id')],
            ['start','<=',date('H:i:s')],
            ['end','>=',date('H:i:s')]
        ];
        $list = Db::table('classes')->where($map)->find();
        $res['start'] = date('Y-m-d').' '.$list['start'];
        $res['end'] = date('Y-m-d').' '.$list['end'];
//        dump($res);
        //查询该时间的订单信息
        $where = [
            ['building_id','=',session('building_id')],
            ['create_time','>=',strtotime($res['start'])],
            ['create_time','<=',strtotime($res['end'])]
        ];
        //入住记录金额
        $res['move'] = Db::table('home_room_income')->where($where)->sum('income_details');
        $res['deposit_record'] = Db::table('home_room_income')->where($where)->sum('deposit_record');
        //退房记录
        $refund = Db::table('home_room_refund')->where($where)->select();
//        dump($refund);
        //预定记录
        $res['home_subscribe_msg'] = Db::table('home_subscribe_msg')->where($where)->sum('income_details');
        //会员充值记录
        $res['member_record'] = Db::table('home_member_record')->where($where)->sum('money');

        //商品消费记录
        $map = [
            ['b.building_id','=',session('building_id')]
        ];
        $res['goods_shop'] =  Db::table('consume')
                            ->alias('a')
                            ->field('a.*,b.room_num,d.number,d.name,d.price,e.username')
                            ->join('room b','a.room_id = b.id')
                            ->join('goodss d','a.goods_id = d.id')
                            ->join('admin e','a.operator = e.id')
                            ->where($map)
                            ->order('a.create_time', 'asc')
                            ->sum('d.price');
        $map1 = [
            ['building_id','=',session('building_id')],
            ['in_time','>=',strtotime($res['start'])],
            ['in_time','<=',strtotime($res['end'])]
        ];
        $res['room_count'] = Db::table('room')->where($map1)->count();
        $map2 = [
            ['building_id','=',session('building_id')],
            ['status','=','1'],

        ];
        $res['free_count'] = Db::table('room')->where($map2)->count();
//        dump($res);

        $payment = $this->select_all('payment');//支付方式
//        $this->html2fpdf();
        return view('index',['res'=>$res,'payment'=>$payment]);
    }

    /*
     * 交接记录
     * */
    public function record(){
        if(request()->isAjax()){
            $data = input('param.');
            $data['create_time'] = time();
            $data['operator'] = session('admin_id');
            $data['building_id'] = session('building_id');

            if(Db::table('home_handover')->insert($data)){
                return $this->return_json('新增成功','100');
            }else{
                return $this->return_json('新增失败','0');
            }
        }
        if(!isset($map)){
            $map = [
                ['building_id','=',session('building_id')]
            ];
        }
        $list = Db::table('home_handover')
            ->where($map)
            ->paginate(['list_rows'=> 15,'query' => input('param.')]);
        return view('record',['list'=>$list]);
    }

    /*
     * 查看记录
     * */
    public function show(){
        $list = Db::table('home_handover')->where('id',input('id'))->find();
        $src = $list['img'];
        echo "<img src='{$src}'>";
    }

    /*
     *
     * */
    public function html2fpdf()
    {
        require_once '../vendor/autoload.php' ;

        $html = "抓取html页面作为字符串赋值到 例如:<h1>我是html中的一个标题</h1> 可以直接使用file_get_content()获取";
        //定义url
        $url = 'http://test.mbdede.com/addons/ewei_hotel/admin/index.php?s=/Home/Web/Newindex/index.html';
        $strContent =file_get_contents($url);
        dump($strContent);
        $mpdf = new Mpdf();
        //设置中文字体
        $mpdf->autoScriptToLang = true;
        $mpdf->autoLangToFont = true;
        $mpdf->WriteHTML($strContent);
        //直接展示
        $mpdf -> Output();
        //保存文件
        $mpdf -> Output('路径和文件名称.pdf');
        $mpdf->Image('frontcover.jpg', 0, 0, 210, 297, 'jpg', '', true, false);

    }


}
