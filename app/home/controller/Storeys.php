<?php
namespace app\home\controller;

use app\BaseController;
use app\index\controller\Basics;
use app\index\validate\Storey;
use think\facade\Db;

/*
 * 楼层设置
 *
 * */
class Storeys extends Basics
{

    // 初始化
    protected function initialize()
    {
        //初始化模型
        $this->model_name = 'Storey';
        $this->new_model();
        $this->validate = new Storey();
        parent::initialize();
    }

    /*
     * 楼层设置
     *
     * */
    public function index()
    {
        $list = Db::table('storey')
            ->where('building',session('building_id'))
            ->paginate(['list_rows'=> 15,'query' => input('param.')]);
        return view('index',['list' => $list]);
    }

    public function adds()
    {
        $data = input('param.');
        $data['create_time'] = time();
        $data['building'] = session('building_id');
        //验证数据
        if(!$this->validate->check($data)){
            return $this->return_json($this->validate->getError(),'0');
        }

        //判断是否添加成功
        if(Db::name('storey')->insert($data)){
            return $this->return_json('新增成功','100');
        }else{
            return $this->return_json('新增失败','0');
        }
    }

}
