<?php
namespace app\index\controller;
use app\BaseController;
/*
 * api 接口
 *
 * */
use Payment\Client;
use Payment\Exceptions;

class Api extends BaseController
{

    /*
     * 身份证接口
     * */
    public function identity_api()
    {

    }

    /*
     * 短信接口
     * */
    public function  message_api($text,$tel)
    {
        $statusStr = array(
            "0" => "短信发送成功",
            "-1" => "参数不全",
            "-2" => "服务器空间不支持,请确认支持curl或者fsocket，联系您的空间商解决或者更换空间！",
            "30" => "密码错误",
            "40" => "账号不存在",
            "41" => "余额不足",
            "42" => "帐户已过期",
            "43" => "IP地址限制",
            "50" => "内容含有敏感词"
        );
        $smsapi = "http://api.smsbao.com/";
        $user = "sehochen"; //短信平台帐号
        $pass = md5("请填写你的"); //短信平台密码
        $content=$text;//要发送的短信内容
        $phone = $tel;//要发送短信的手机号码
        $sendurl = $smsapi."sms?u=".$user."&p=".$pass."&m=".$phone."&c=".urlencode($content);
        $result =file_get_contents($sendurl) ;
//        echo $statusStr[$result];
        dump($statusStr[$result]);
//        return $statusStr[$result];
    }

    /*
     * 发送短信
     * */
    public function send_sms($code,$tel){
        $text = "你的注册码为".$code.',请尽快使用！';
        $this->message_api($text,$tel);
    }

    /*
     * 快递物流接口
     * */
    public function  express_api()
    {

    }

    /*
     * 快递物流接口
     * */
    public function  travel_api()
    {

    }


    /*
     * 支付宝
     * */
    /**
     *
     */
    public function alipay_app(){

        require_once '../vendor/autoload.php' ;
        date_default_timezone_set('Asia/Shanghai');

        //读取支付宝配置信息
        $aliConfig=config('aliconfig');

        // 交易信息
        $tradeNo = time() . rand(1000, 9999);
        $payData = [
            'body'         => 'ali wap pay',
            'subject'      => '测试支付宝手机网站支付',
            'trade_no'     => $tradeNo,
            'time_expire'  => time() + 600, // 表示必须 600s 内付款
            'amount'       => '0.01', // 单位为元 ,最小为0.01
            'return_param' => 'tata', // 一定不要传入汉字，只能是 字母 数字组合
            // 'client_ip' => isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '127.0.0.1',// 客户地址
            'goods_type' => '1', // 0—虚拟类商品，1—实物类商品
            'store_id'   => '',
            'quit_url'   => 'https://dayutalk.cn', // 收银台的返回按钮（用户打断支付操作时返回的地址,4.0.3版本新增）
        ];

        // 使用
        try {

            $client = new Client(Client::ALIPAY, $aliConfig);
            $res    = $client->pay(Client::ALI_CHANNEL_WAP, $payData);
        } catch (InvalidArgumentException $e) {
            echo $e->getMessage();
            exit;
        } catch (GatewayException $e) {
            echo $e->getMessage();
            var_dump($e->getRaw());
            exit;
        } catch (ClassNotFoundException $e) {
            echo $e->getMessage();
            exit;
        } catch (Exception $e) {
            echo $e->getMessage();
            exit;
        }

        header('Location:' . $res);



    }

}
